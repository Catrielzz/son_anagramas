package Java;

public class SonAnagramas {
    public  static void main(String[] args) {

        String frase1 = "hola amor"; // .sort = " aahlmoor"
        String frase2 = "halo roma"; // .sort = " aahlmoor"

        char[] ordenada1 = frase1.toCharArray(); // convertimos la frase 1 en un array de caracteres
        java.util.Arrays.sort(ordenada1); // ordenamos los caracteres
        String cadena1 = new String(ordenada1);
        // System.out.println(cadena1); // muestra los caracteres ordenados

        char[] ordenada2 = frase2.toCharArray();
        java.util.Arrays.sort(ordenada2);
        String cadena2 = new String(ordenada2);
        // System.out.println(cadena2);

        if (cadena1.equals(cadena2)){
            System.out.println("Son anagramas");
        } else {
            System.out.println("No son anagramas");
        }

    }
}
